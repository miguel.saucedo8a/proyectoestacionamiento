﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using ESTACIONAMIENTO.Model;

namespace ESTACIONAMIENTO
{
    public partial class Reportes : System.Web.UI.Page
    {
        DBConnection DB = new DBConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorBusqueda.Visible = false;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorBusqueda.Visible = false;

                if (txtfecha.Text != "" && txtHora1.Text == "" && txtHora2.Text == "")
                {
                    Methods metodo = new Methods();
                    string Consulta = @"SELECT placaVehiculo AS 'Núm. Placa' , tiempo AS 'Tiempo', tipo_vehiculo AS 'Tipo Vehículo', totalPagar AS 'Total a Pagar' FROM vehiculo V
                                            INNER JOIN tipovehiculo TV ON TV.idTipoVehiculo = V.idTipoVehiculo 
                                            WHERE CONVERT(DATE, horaEntrada) = '" + txtfecha.Text + "'";
                    DataTable returnDatos = metodo.ConsultaGeneral(Consulta);

                    gvReporte.DataSource = returnDatos;
                    gvReporte.DataBind();
                }
                else if (txtfecha.Text != "" && txtHora1.Text != "" && txtHora2.Text != "")
                {
                    Methods metodo = new Methods();
                    string Consulta = @"SELECT placaVehiculo AS 'Núm. Placa' , tiempo AS 'Tiempo', tipo_vehiculo AS 'Tipo Vehículo', totalPagar AS 'Total a Pagar' FROM vehiculo V
                                            INNER JOIN tipovehiculo TV ON TV.idTipoVehiculo = V.idTipoVehiculo 
                                            WHERE CONVERT(DATE, horaEntrada) = '" + txtfecha.Text + "' AND CONVERT(time, horaEntrada) BETWEEN '" + txtHora1.Text + "' AND '" + txtHora2.Text + "'";
                    DataTable returnDatos = metodo.ConsultaGeneral(Consulta);

                    gvReporte.DataSource = returnDatos;
                    gvReporte.DataBind();
                }
                else
                {
                    ErrorBusqueda.Visible = true;
                    ErrorBusqueda.InnerText = "Faltan campos por llenar";
                }
            }
            catch { }
        }
    }
}