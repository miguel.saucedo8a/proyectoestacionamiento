﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Control.Master" AutoEventWireup="true" CodeBehind="vehiculos.aspx.cs" Inherits="ESTACIONAMIENTO.vehiculos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <h1>SISTEMA DE REGISTRO DE ESTACIONAMIENTO</h1>
        
        <br />
        <h3><asp:Label ID="Label1" runat="server" Text="Entrada de Vehículo"></asp:Label></h3>
        <div>
            <Table >
                <tr>
                    <td>Número de placa</td>
                    <td> <asp:TextBox ID="txtPlaca" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Tipo de Vehículo</td>
                    <td>
                        <asp:DropDownList ID="SelectTipoVehiculo" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                </tr>
            </Table>
            <asp:Button ID="btnIngresar" runat="server" Text="Registrar Entrada" OnClick="btnIngresar_Click" />
            <br />
            <div>
                <p  id="DatosRegistrados" runat="server" style="background-color:#dff0d8;"></p>
            </div>
            <div>
                <p  id="ErrorRegistro" runat="server" style="background-color:#f2dede;"></p>
            </div>
        </div>

        <br />
        <h3><asp:Label ID="Label2" runat="server" Text="Salida de Vehículo"></asp:Label></h3>
        <div>
            <Table >
                <tr>
                    <td>Número de placa</td>
                    <td><asp:TextBox ID="txtPlacaSalida" runat="server"></asp:TextBox></td>
                    <td><asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" /></td>
                </tr>
                <div id="SalidaVehiculo" runat="server">
                    <tr>
                        <td>Tipo de Vehículo</td>
                        <td><asp:TextBox ID="txtTipoVehiculo" runat="server" Enabled="false"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Tiempo en Estacionamiento</td>
                        <td><asp:TextBox ID="txtTiempo" runat="server" Enabled="false"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Total a Pagar</td>
                        <td><asp:TextBox ID="txtTotalPagar" runat="server" Enabled="false"></asp:TextBox></td>
                    </tr>
                </div>
                <tr>
                    <td><asp:Button ID="btnSalidaVehiculo" runat="server" Text="Registrar Salida" OnClick="btnSalidaVehiculo_Click" /></td>
                    <td><asp:Button ID="Cancelar" runat="server" Text="Cancelar" OnClick="Cancelar_Click" /></td>
                </tr>
                <asp:Label ID="idDatos" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="tiempoSalida" runat="server" Text="" Visible="true"></asp:Label>

            </Table>

            <div>
                <p  id="correctoSalida" runat="server" style="background-color:#dff0d8;"></p>
            </div>
            <div>
                <p  id="errorSalida" runat="server" style="background-color:#f2dede;"></p>
            </div>


        </div>

</asp:Content>
