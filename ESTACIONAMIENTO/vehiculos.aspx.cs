﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//LIBRERIAS
using System.Data.SqlClient;
using System.Data;
using ESTACIONAMIENTO.Model;

namespace ESTACIONAMIENTO
{
    public partial class vehiculos : System.Web.UI.Page
    {
        DBConnection DB = new DBConnection(); //Manda llamar la clase de la cadena de conexion
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Attributes.Add("autocomplete", "off");
            DatosRegistrados.Visible = false;
            ErrorRegistro.Visible = false;

            correctoSalida.Visible = false;
            errorSalida.Visible = false;

            SalidaVehiculo.Visible = false;

            if (!IsPostBack)
            {
                CargarTipoVehiculo();
            }

        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                Methods metodo = new Methods();
                DataTable dtPlaca = new DataTable();
                dtPlaca = metodo.ReadPlacas(txtPlaca.Text);
                if (dtPlaca != null)
                {
                    if (dtPlaca.Rows.Count > 0)
                    {
                        DatosRegistrados.Visible = false;
                        ErrorRegistro.Visible = true;
                        ErrorRegistro.InnerText = "El vehículo con la placa " + txtPlaca.Text + " se encuentra dentro del estacionamiento";
                    }
                    else
                    {

                        if (txtPlaca.Text != "" && SelectTipoVehiculo.SelectedIndex != 0)
                        {
                            DateTime fechaActual = DateTime.Now;
                            DB.dbConnection.ConnectionString = DB.dbString; //Realiza la conexion con la BD
                            DB.dbConnection.Open();
                            SqlCommand cmd = new SqlCommand("insertVehiculo", DB.dbConnection);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@placaVehiculo", SqlDbType.VarChar).Value = txtPlaca.Text;
                                cmd.Parameters.AddWithValue("@horaEntrada", SqlDbType.DateTime).Value = fechaActual;
                                cmd.Parameters.AddWithValue("@horaSalida", SqlDbType.VarChar).Value = DBNull.Value;
                                cmd.Parameters.AddWithValue("@tiempo", SqlDbType.VarChar).Value = 0;
                                cmd.Parameters.AddWithValue("@totalPagar", SqlDbType.Decimal).Value = 0;
                                cmd.Parameters.AddWithValue("@idTipoVehiculo", SqlDbType.Int).Value = SelectTipoVehiculo.SelectedValue;
                            }
                            cmd.ExecuteNonQuery();
                            DB.dbConnection.Close();

                            DatosRegistrados.Visible = true;
                            ErrorRegistro.Visible = false;
                            DatosRegistrados.InnerText = "El vehículo se ingreso correctamente";

                            txtPlaca.Text = "";
                            SelectTipoVehiculo.ClearSelection();
                        }
                        else
                        {
                            ErrorRegistro.Visible = true;
                            DatosRegistrados.Visible = false;
                            ErrorRegistro.InnerText = "Faltan datos por llenar";
                        }
                    }
                }
            }
            catch(Exception)
            {
            }
        }

        private void CargarTipoVehiculo()
        {
            try
            {
                DB.dbConnection.ConnectionString = DB.dbString; //Realiza la conexion con la BD
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT idTipoVehiculo, tipo_vehiculo FROM tipovehiculo";
                cmd.Connection = DB.dbConnection;
                DB.dbConnection.Open();
                SelectTipoVehiculo.DataSource = cmd.ExecuteReader();
                SelectTipoVehiculo.DataTextField = "tipo_vehiculo";
                SelectTipoVehiculo.DataValueField = "idTipoVehiculo";
                SelectTipoVehiculo.DataBind();
                SelectTipoVehiculo.Items.Insert(0, new ListItem("[Seleccionar]", "0"));

                ErrorRegistro.Visible = false;
                DatosRegistrados.Visible = false;

            }
            catch { }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPlacaSalida.Text != "")
                {
                    SalidaVehiculo.Visible = true;

                    Methods metodo = new Methods();
                    string ConsultaHoraE = @"SELECT horaEntrada, tipo_vehiculo, precioMinuto, idVehiculo FROM vehiculo V
                                        INNER JOIN tipovehiculo TV on TV.idTipoVehiculo = V.idTipoVehiculo
                                        where horaSalida is NULL AND placaVehiculo = '" + txtPlacaSalida.Text + "'";
                    DataTable returnDatos = metodo.ConsultaGeneral(ConsultaHoraE);

                    idDatos.Text = returnDatos.Rows[0][3].ToString();
                    DateTime fechaActual = DateTime.Now;
                    tiempoSalida.Text = fechaActual.ToString();

                    TimeSpan diferenciaHrs = fechaActual.Subtract(Convert.ToDateTime(returnDatos.Rows[0][0].ToString()));
                    int difHrs = diferenciaHrs.Hours;
                    int difMin = diferenciaHrs.Minutes;

                    txtTiempo.Text = (difHrs > 0) ? difHrs + " hrs " + difMin + " min" : difMin + " min";
                    txtTipoVehiculo.Text = returnDatos.Rows[0][1].ToString();

                    double hrs_min = (difHrs * 60) + difMin;
                    double total = Convert.ToDouble(returnDatos.Rows[0][2]) * hrs_min;
                    txtTotalPagar.Text = "$ " + total.ToString();

                }
            }
            catch
            {
            }
        }

        protected void btnSalidaVehiculo_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPlacaSalida.Text != "")
                {
                    string updateVehiculo = "UPDATE vehiculo SET tiempo = '" + txtTiempo.Text + "', horaSalida = '" + Convert.ToDateTime(tiempoSalida.Text) + "', totalPagar = '" + txtTotalPagar.Text + "' WHERE placaVehiculo = '" + txtPlacaSalida.Text + "' AND idVehiculo = '" + Convert.ToInt32(idDatos.Text) + "'";
                    int update = Methods.Update_Delete(updateVehiculo);

                    correctoSalida.Visible = true;
                    errorSalida.Visible = false;
                    correctoSalida.InnerText = "Se registro correctamente la salida del vehículo con la placa " + txtPlacaSalida.Text;

                    tiempoSalida.Text = "";
                    txtPlacaSalida.Text = "";
                }

            }
            catch
            {
            }
        }

        protected void Cancelar_Click(object sender, EventArgs e)
        {
            SalidaVehiculo.Visible = false;
            idDatos.Text = "";
            tiempoSalida.Text = "";
            txtPlacaSalida.Text = "";
        }
    }
}