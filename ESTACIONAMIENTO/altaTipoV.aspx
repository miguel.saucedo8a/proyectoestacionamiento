﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Control.Master" AutoEventWireup="true" CodeBehind="altaTipoV.aspx.cs" Inherits="ESTACIONAMIENTO.altaTipoV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
            <h2>ALTA NUEVOS VEHICULOS</h2>
        <br />
        <div>
            <Table >
                <tr>
                    <td>Tipo de Vehículo</td>
                    <td> <asp:TextBox ID="txtTipoVehiculo" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Precio por Minuto</td>
                    <td> <asp:TextBox ID="txtPrecioMinuto" runat="server" TextMode="Number"></asp:TextBox></td>
                </tr>
            </Table>
            <br />
            <asp:Button ID="btnGuardar" runat="server" Text="Registrar Vehículo" OnClick="btnGuardar_Click" />

            <div>
                <p  id="DatosRegistrados" runat="server" style="background-color:#dff0d8;"></p>
            </div>
            <div>
                <p  id="ErrorRegistro" runat="server" style="background-color:#f2dede;"></p>
            </div>
        </div>

        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal">
                <Columns>
                    <asp:BoundField DataField="tipo_vehiculo" HeaderText="TIPO DE VEHICULO" SortExpression="tipo_vehiculo" />
                    <asp:BoundField DataField="precioMinuto" HeaderText="PRECIO POR MINUTO MXN" SortExpression="precioMinuto" />
                </Columns>
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ESTACIONAMIENTOConnectionString %>" SelectCommand="SELECT [tipo_vehiculo], [precioMinuto] FROM [tipovehiculo]"></asp:SqlDataSource>
        </div>


</asp:Content>
