﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ESTACIONAMIENTO.Model;
using System.Data;
using System.Data.SqlClient;

namespace ESTACIONAMIENTO
{
    public partial class altaTipoV : System.Web.UI.Page
    {
        DBConnection DB = new DBConnection(); 

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Attributes.Add("autocomplete", "off");
            DatosRegistrados.Visible = false;
            ErrorRegistro.Visible = false;
            
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Methods metodo = new Methods();
                DataTable tipoVeh = new DataTable();
                tipoVeh = metodo.ReadTipoVeh(txtTipoVehiculo.Text);
                if (tipoVeh != null)
                {
                    if (tipoVeh.Rows.Count > 0)
                    {
                        DatosRegistrados.Visible = false;
                        ErrorRegistro.Visible = true;
                        ErrorRegistro.InnerText = "Vehículo tipo " + txtTipoVehiculo.Text + " ya esta registrado";
                    }
                    else
                    {

                        if (txtTipoVehiculo.Text != "" && txtPrecioMinuto.Text != "")
                        {
                            DB.dbConnection.ConnectionString = DB.dbString;
                            DB.dbConnection.Open();
                            SqlCommand cmd = new SqlCommand("insertTipoVehiculo", DB.dbConnection);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@tipo_vehiculo", SqlDbType.VarChar).Value = txtTipoVehiculo.Text;
                                cmd.Parameters.AddWithValue("@precioMinuto", SqlDbType.Decimal).Value = Convert.ToDecimal(txtPrecioMinuto.Text);
                            }
                            cmd.ExecuteNonQuery();
                            DB.dbConnection.Close();

                            DatosRegistrados.Visible = true;
                            ErrorRegistro.Visible = false;
                            DatosRegistrados.InnerText = "Vehículo registrado correctamente";

                            GridView1.DataBind();

                            txtTipoVehiculo.Text = "";
                            txtPrecioMinuto.Text = "";
                        }
                        else
                        {
                            ErrorRegistro.Visible = true;
                            DatosRegistrados.Visible = false;
                            ErrorRegistro.InnerText = "Faltan datos por llenar";
                        }
                    }
                }
            }
            catch 
            {
            }
        }
    }
}
