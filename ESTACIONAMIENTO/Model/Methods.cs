﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//LIBRERIAS
using System.Data.SqlClient;
using System.Data;

namespace ESTACIONAMIENTO.Model
{
    public class Methods
    {
        public DataTable ConsultaGeneral(string _query)
        {
            DataTable dtResultado = new DataTable();
            DBConnection DB = new DBConnection();
            try
            {
                DB.dbConnection.ConnectionString = DB.dbString;
                DB.dbConnection.Open();
                SqlDataAdapter sql = new SqlDataAdapter(_query, DB.dbConnection);
                sql.Fill(dtResultado);
                DB.dbConnection.Close();

                return dtResultado;
            }
            catch (Exception)
            {
                return dtResultado;
            }
        }

        public static int Update_Delete(string query)
        {
            try
            {
                DBConnection DB = new DBConnection();
                DB.dbConnection.ConnectionString = DB.dbString;
                DB.dbConnection.Open();
                DB.dbCommand.CommandText = query;
                DB.dbCommand.Connection = DB.dbConnection;
                int result = DB.dbCommand.ExecuteNonQuery();
                DB.dbConnection.Close();

                return result;
            }
            catch (Exception)
            {
                int result = 0;
                return result;
            }
        }

        public DataTable ReadPlacas(string placa)
        {
            string query = null;
            DataTable dtResult = new DataTable();
            DBConnection DB = new DBConnection(); 

            DB.dbConnection.ConnectionString = DB.dbString; //Realiza la conexion con la BD
            DB.dbConnection.Open();
            if (placa != null)
            {
                query = "SELECT placaVehiculo FROM vehiculo where horaSalida is NULL and placaVehiculo = '" + placa + "'";
            }

            SqlDataAdapter resultadoQuery = new SqlDataAdapter(query, DB.dbConnection);
            resultadoQuery.Fill(dtResult);
            DB.dbConnection.Close();
            return dtResult;
        }

        public DataTable ReadTipoVeh(string tipoVeh)
        {
            string query = null;
            DataTable dtResult = new DataTable();
            DBConnection DB = new DBConnection();

            DB.dbConnection.ConnectionString = DB.dbString; //Realiza la conexion con la BD
            DB.dbConnection.Open();
            if (tipoVeh != null)
            {
                query = "SELECT tipo_vehiculo FROM tipovehiculo where tipo_vehiculo = '" + tipoVeh  + "'";
            }

            SqlDataAdapter resultadoQuery = new SqlDataAdapter(query, DB.dbConnection);
            resultadoQuery.Fill(dtResult);
            DB.dbConnection.Close();
            return dtResult;
        }

    }
}