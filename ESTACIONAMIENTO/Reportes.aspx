﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Control.Master" AutoEventWireup="true" CodeBehind="Reportes.aspx.cs" Inherits="ESTACIONAMIENTO.Reportes" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h2>GENERAR REPORTES</h2>

    <h3>Los filtros pueden ser por fecha ó fecha y horas</h3>

    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Seleccione fecha"></asp:Label>
            </td>
            <td> 
                <asp:TextBox ID="txtfecha" runat="server" TextMode="Date"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Primer rango de hora"></asp:Label>
            </td>
           <td>
                <asp:Label ID="Label4" runat="server" Text="Segundo rango de hora"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtHora1" runat="server" TextMode="Time"></asp:TextBox>
            </td>
            <td> 
                <asp:TextBox ID="txtHora2" runat="server" TextMode="Time"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />

    <div>
        <p  id="ErrorBusqueda" runat="server" style="background-color:#f2dede;"></p>
    </div>


    <br />

    <asp:GridView ID="gvReporte" runat="server">
    </asp:GridView>
    
</asp:Content>
