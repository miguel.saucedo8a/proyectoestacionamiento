CREATE DATABASE ESTACIONAMIENTO

CREATE TABLE tipovehiculo(
idTipoVehiculo INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
tipo_vehiculo VARCHAR(30),
precioMinuto NUMERIC(6,2)
)

CREATE TABLE vehiculo(
idVehiculo INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
placaVehiculo VARCHAR(20) NOT NULL,
horaEntrada DATETIME NOT NULL,
horaSalida VARCHAR(50),
tiempo VARCHAR(20),
totalPagar varchar(15),
idTipoVehiculo INT NOT NULL,
FOREIGN KEY (idTipoVehiculo) REFERENCES tipovehiculo(idTipoVehiculo)
)


INSERT INTO tipovehiculo (tipo_vehiculo, precioMinuto) VALUES ('Oficial' , 0)
INSERT INTO tipovehiculo (tipo_vehiculo, precioMinuto) VALUES ('Residente' , 1.00)
INSERT INTO tipovehiculo (tipo_vehiculo, precioMinuto) VALUES ('No Residente' , 3.00)

--PROCEDIMIENTO ALMACENADO INSERT VEHICULOS
CREATE PROCEDURE insertVehiculo
@placaVehiculo VARCHAR(20),
@horaEntrada DATETIME,
@horaSalida VARCHAR(50),
@tiempo VARCHAR(20),
@totalPagar varchar(15),
@idTipoVehiculo INT
AS
	INSERT INTO vehiculo (placaVehiculo, horaEntrada, horaSalida, tiempo, totalPagar, idTipoVehiculo) 
	VALUES  (@placaVehiculo, @horaEntrada, @horaSalida, @tiempo, @totalPagar, @idTipoVehiculo)

CREATE PROCEDURE insertTipoVehiculo
@tipo_vehiculo VARCHAR(30),
@precioMinuto NUMERIC(6,2)
AS
INSERT INTO tipovehiculo (tipo_vehiculo, precioMinuto) VALUES (@tipo_vehiculo, @precioMinuto)



